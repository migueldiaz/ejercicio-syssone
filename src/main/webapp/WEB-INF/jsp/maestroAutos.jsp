<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="jc"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="../../webjars/bootstrap/4.2.1/css/bootstrap.min.css"
	rel="stylesheet">
<script src="../../webjars/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script src="../../webjars/jquery/3.0.0/jquery.min.js"></script>
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
	integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
	crossorigin='anonymous'>
<title>Maestro de Automovil</title>
</head>
<body>
	<div class="container">

		<div class="row" style="margin-top: 20px;">
			<div class="col" style="text-align: left;">
			<tags:url value="/fabrica" var="URLinic" />
				<a href="${URLinic}" class="btn btn-info btn-lg"> <i
					class="fas fa-home" style="font-size: 24px"></i>Inicio
				</a>
			</div>
			<div class="col" style="text-align: center;">
				<h1>Automoviles</h1>
			</div>
			<tags:url value="/automoviles/addautos" var="URLAdd" />
			<div class="col" style="text-align: right; margin-top: 5px;">
				<a href="${URLAdd}" class="btn btn-info btn-lg"> <i
					class="fa fa-plus-square" style="font-size: 24px"></i> A�adir
				</a>
			</div>
		</div>


		<div class="row">

			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Modelo</th>
						<th scope="col">Precio</th>
						<th scoppe="col">Acciones</th>

					</tr>
				</thead>
				<tbody>
					<%
						int cont = 1;
					%>
					<jc:forEach items="${listaAuto}" var="listaAutos">

						<tr>
							<th scope="row"><%=cont++%></th>
							<td>${listaAutos.modelo }</td>
							<td>${listaAutos.precio }</td>

							<td><tags:url
									value="/automoviles/editautos/${listaAutos.idautomovil }"
									var="URLUpdate" /> <a class="btn btn-success"
								href="${URLUpdate }"><i class='fas fa-edit'
									style='font-size: 24px'></i></a> <tags:url
									value="/automoviles/deleteautos/${listaAutos.idautomovil }"
									var="URLDelete" /> <a class="btn btn-danger"
								href="${URLDelete }"><i class='fas fa-trash'
									style='font-size: 24px'></i></a></td>
						</tr>
					</jc:forEach>
				</tbody>

			</table>

		</div>
	</div>

</body>
</html>