<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="jc"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro de Automoviles</title>
<link href="../../webjars/bootstrap/4.2.1/css/bootstrap.min.css"
	rel="stylesheet">
<script src="../../webjars/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script src="../../webjars/jquery/3.0.0/jquery.min.js"></script>
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
	integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
	crossorigin='anonymous'>
</head>
<body>

	<div class="container">

		<nav class="navbar navbar-light bg-light center"> <a
			class="navbar-brand" href="#" style="text-align: center;">Total
			Cotizado de Automoviles</a> </nav>

		<div class="row">

			<div class="form-group col-md-4 offset-md-4">
				<h1>Automovil B�sico</h1>
				<h3>Modelo ${transaccion.idautomovil.modelo }</h3>
				<h4>Total Cotizado ${total }</h4>
			</div>

		</div>

		<div class="col-md-4 offset-md-4">
			<tags:url value="/fabrica" var="URLUlist" />
			<a class="btn btn-danger btn-lg btn-block" href="${URLUlist }">Volver a Cotizar</a>


		</div>
</body>
</html>