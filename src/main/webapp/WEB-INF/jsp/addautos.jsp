<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="jc"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro de Automoviles</title>
<link href="../../webjars/bootstrap/4.2.1/css/bootstrap.min.css"
	rel="stylesheet">
<script src="../../webjars/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script src="../../webjars/jquery/3.0.0/jquery.min.js"></script>
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
	integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
	crossorigin='anonymous'>
</head>
<body>

	<div class="container">

		<div class="col-md-6 offset-md-4">
			<h2>Registro de Automoviles</h2>
		</div>
		<tags:url value="/automoviles/saveautos" var="URLSave" />


		<form:form modelAttribute="autosForm" method="post"
			action="${URLSave }" cssClass="form">
			<form:hidden path="idautomovil" />


			<div class="form-row">


				<div class="col-md-4 offset-md-4">
					<form:label path="">Modelo</form:label>
					<form:input path="modelo" type="text" cssClass="form-control"
						id="documento" />
				</div>

				<div class="col-md-4 offset-md-4">
					<form:label path="">Precios</form:label>
					<form:input path="precio" type="number" cssClass="form-control"
						id="nombre" />
				</div>

			</div>
			<p></p>

			<div class="col-md-4 offset-md-4">
				<button class="btn btn-primary btn-lg btn-block" type="submit">Enviar</button>
			</div>
		</form:form>
		<p></p>

		<div class="col-md-4 offset-md-4">
			<tags:url value="/automoviles/mautos" var="URLUlist" />
			<a class="btn btn-danger btn-lg btn-block" href="${URLUlist }">Cancelar</a>
		</div>

	</div>

</body>
</html>