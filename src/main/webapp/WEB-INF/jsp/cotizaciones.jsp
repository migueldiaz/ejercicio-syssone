<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="jc"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro de Automoviles</title>
<link href="../../webjars/bootstrap/4.2.1/css/bootstrap.min.css"
	rel="stylesheet">
<script src="../../webjars/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script src="../../webjars/jquery/3.0.0/jquery.min.js"></script>
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
	integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
	crossorigin='anonymous'>
</head>
<body>

	<div class="container">
		<nav class="navbar navbar-light bg-light center center"> <a
			class="navbar-brand" href="#">Cotizaciones de Automoviles</a> </nav>

		<tags:url value="/fabrica/savecotizacion" var="URLSave" />
		<form:form modelAttribute="cotizarForm" method="post"
			action="${URLSave }" cssClass="form">
			<form:hidden path="idtransaccion" />
>

			<div class="row">

				<div class="form-group col-md-4 offset-md-4">
					<form:label path="">Automoviles</form:label>
					<form:select path="idautomovil" cssClass="form-control "
						id="exampleFormControlSelect1">
						<option value="">Seleccione</option>
						<jc:forEach items="${listaAuto }" var="autos">
							<option value="${autos.idautomovil }">${autos.modelo }</option>
						</jc:forEach>
					</form:select>

				</div>
			</div>

			<div class="row">

				<h1>${opciones.idopcionales }</h1>

				<jc:forEach items="${listopcionales }" var="opciones">
					<div class="col-auto my-1 col-md-4 offset-md-4">
						<div class="custom-control custom-checkbox mr-sm-2">
							<form:radiobutton path="idopcionales"
								name="${opciones.abreviatura }" cssClass="custom-control-input"
								id="customControlAutosizing${opciones.idopcionales }"
								value="${opciones.idopcionales }" />
							<label class="custom-control-label"
								for="customControlAutosizing${opciones.idopcionales }">${opciones.tipovariante }
							</label>
						</div>
					</div>
				</jc:forEach>

			</div>

			<div class="col-md-4 offset-md-4">
				<button class="btn btn-primary btn-lg btn-block" type="submit">Enviar</button>
			</div>

		</form:form>
		<div class="col-md-4 offset-md-4" style="margin-top: 20px;">
			<tags:url value="/fabrica" var="URLUlist" />
			<a class="btn btn-danger btn-lg btn-block" href="${URLUlist }">Cancelar</a>
		</div>

	</div>
</body>
</html>