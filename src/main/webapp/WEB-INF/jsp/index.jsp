<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="jc"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro de Variantes</title>
<link href="../../webjars/bootstrap/4.2.1/css/bootstrap.min.css"
	rel="stylesheet">
<script src="../../webjars/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script src="../../webjars/jquery/3.0.0/jquery.min.js"></script>
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
	integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
	crossorigin='anonymous'>
</head>
<body>
	<div class="container">

		<!-- As a link -->
		<nav class="navbar navbar-light bg-light"> <a
			class="navbar-brand" href="#">Fabrica de Automoviles</a> </nav>

		<div class="row col-12" style="margin-top: 80px;">
			<div class="row">
				<div class="col-sm-4">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Automoviles</h5>
							<p class="card-text">Puede Crear, Actualizar y Eliminar
								Modelos Automoviles.</p>
							<tags:url value="/automoviles/mautos" var="URLUlist" />
							<a href="${URLUlist }" class="btn btn-primary">ir a
								Automoviles</a>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Variantes Opcionales</h5>
							<p class="card-text">Puede Crear, Actualizar y Eliminar Tipo
								de Variantes.</p>
							<tags:url value="/opcionales/listopcionales" var="URLopc" />
							<a href="${URLopc }" class="btn btn-primary">ir a Opcionales</a>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Cotizaciones</h5>
							<p class="card-text">Puede Visualizar el Costo Total del
								Automoviles.</p>
							<tags:url value="/fabrica/cotizacion" var="URLcot" />
							<a href="${URLcot }" class="btn btn-primary">ir a
								Cotizaciones</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>
</html>