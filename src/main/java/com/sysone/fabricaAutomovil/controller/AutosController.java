package com.sysone.fabricaAutomovil.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sysone.fabricaAutomovil.model.Automovil;
import com.sysone.fabricaAutomovil.services.AutomovilServices;


@Controller
@RequestMapping(value="/automoviles")
public class AutosController {
	
	@Autowired
	AutomovilServices automovilServices;
	
	
	@RequestMapping(value="/mautos", method=RequestMethod.GET)
	public ModelAndView maestroAutos(){
		ModelAndView mva = new ModelAndView("maestroAutos");
		List<Automovil> listAuto = automovilServices.getListaAutomovil();
		mva.addObject("listaAuto", listAuto);

		return mva;		
	}
	
	@RequestMapping(value="/addautos", method=RequestMethod.GET)
	public ModelAndView addAutos(){
		ModelAndView model = new ModelAndView();
		Automovil autos = new Automovil();
		model.addObject("autosForm", autos);
		model.setViewName("addautos");
		
		return model;
		
	}
	
	@RequestMapping(value="/editautos/{idautomovil}", method=RequestMethod.GET)
	public ModelAndView editUsers(@PathVariable("idautomovil") Long idAutomovil){
	ModelAndView modeledit = new ModelAndView();
	
	Automovil auto = automovilServices.getAutomovilId(idAutomovil);
	modeledit.addObject("autosForm", auto);
	modeledit.setViewName("addautos");
	return modeledit;

	}
	
	@RequestMapping(value="/saveautos", method=RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("autosForm") Automovil auto){
		automovilServices.saveOrUpdate(auto);
		return new ModelAndView("redirect:/automoviles/mautos");
	}
	
	@RequestMapping(value="/deleteautos/{idautomovil}", method=RequestMethod.GET)
	public ModelAndView deleteuser(@PathVariable("idautomovil") Long idAutomovil){
		automovilServices.deleteAutomovilId(idAutomovil);
	return new ModelAndView("redirect:/automoviles/mautos");
		
	}
	
}
