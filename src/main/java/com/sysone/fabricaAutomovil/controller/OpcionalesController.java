package com.sysone.fabricaAutomovil.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sysone.fabricaAutomovil.model.Opcionales;
import com.sysone.fabricaAutomovil.services.OpcionalesServices;

@Controller
@RequestMapping(value="/opcionales")
public class OpcionalesController {

	@Autowired
	OpcionalesServices opcionalesServices;
	
	@RequestMapping(value="/listopcionales", method = RequestMethod.GET)
	public ModelAndView listopcion(){
		ModelAndView mao = new ModelAndView("maestroOpcionales");
		List<Opcionales> listOpcional = opcionalesServices.getlistOpcionales();
		mao.addObject("listopcionales", listOpcional);
		
		return mao;		
	}

	@RequestMapping(value="/addopciones", method=RequestMethod.GET)
	public ModelAndView addAutos(){
		ModelAndView model = new ModelAndView();
		Opcionales opcionales = new Opcionales();
		model.addObject("opcionForm", opcionales);
		model.setViewName("addopcionales");
		
		return model;
		
	}
	
	@RequestMapping(value="/editopciones/{idopcionales}", method=RequestMethod.GET)
	public ModelAndView editopcion(@PathVariable("idopcionales") Long idopcionales){
	ModelAndView modeledit = new ModelAndView();	
	Opcionales opcion = opcionalesServices.getOpcionalesId(idopcionales);
	modeledit.addObject("opcionForm", opcion);
	modeledit.setViewName("addopcionales");
	return modeledit;

	}
	
	@RequestMapping(value="/saveopciones", method=RequestMethod.POST)
	public ModelAndView saveOpciones(@ModelAttribute("opcionForm") Opcionales opcion){
		opcionalesServices.saveOrUpdateOpcionales(opcion);
		return new ModelAndView("redirect:/opcionales/listopcionales");
	}
	
	@RequestMapping(value="/deleteopciones/{idopcionales}", method=RequestMethod.GET)
	public ModelAndView deleteOciones(@PathVariable("idopcionales") Long idopcionales){
		opcionalesServices.deleteOpcionalesId(idopcionales);
	return new ModelAndView("redirect:/opcionales/listopcionales");
		
	}
	
	
}
