package com.sysone.fabricaAutomovil.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.sysone.fabricaAutomovil.model.Automovil;
import com.sysone.fabricaAutomovil.model.Opcionales;
import com.sysone.fabricaAutomovil.model.Transacciones;
import com.sysone.fabricaAutomovil.operaciones.ConsultasTransaccionales;
import com.sysone.fabricaAutomovil.services.AutomovilServices;
import com.sysone.fabricaAutomovil.services.OpcionalesServices;
import com.sysone.fabricaAutomovil.services.TransaccionesServices;

@Controller
@RequestMapping(value = "fabrica")
public class PrincipalController {

	@Autowired
	AutomovilServices automovilServices;

	@Autowired
	OpcionalesServices opcionalesServices;

	@Autowired
	TransaccionesServices transaccionesServices;

	private ConsultasTransaccionales consultasTransaccionales;

	@RequestMapping(value = "")
	ModelAndView principal() {
		ModelAndView map = new ModelAndView("index");

		return map;
	}

	@RequestMapping(value = "/cotizacion")
	ModelAndView cotizacion(@ModelAttribute("cotizarForm") Transacciones transacciones) {
		ModelAndView map = new ModelAndView();
		List<Automovil> listAuto = automovilServices.getListaAutomovil();
		List<Opcionales> listOpcional = opcionalesServices.getlistOpcionales();
		map.addObject("listopcionales", listOpcional);
		map.addObject("listaAuto", listAuto);
		map.setViewName("cotizaciones");

		return map;
	}

	@RequestMapping(value = "/savecotizacion", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("cotizarForm") Transacciones transacciones, BindingResult result,
			SessionStatus status, HttpServletRequest re, Model model) {		
		ConsultasTransaccionales ct = new ConsultasTransaccionales();
		ModelAndView mas = new ModelAndView();
		transaccionesServices.saveOrUpdateTransacciones(transacciones);
		
		mas.addObject("total", ct.totalCotizar(transacciones));
		mas.addObject("transaccion", transacciones);
		mas.setViewName("totalCotizacion");

		
		return mas;
	}
}
