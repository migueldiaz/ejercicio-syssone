package com.sysone.fabricaAutomovil.repository;

import org.springframework.data.repository.CrudRepository;

import com.sysone.fabricaAutomovil.model.Opcionales;

public interface OpcionalesRepository extends CrudRepository<Opcionales, Long> {

}
