package com.sysone.fabricaAutomovil.repository;

import org.springframework.data.repository.CrudRepository;

import com.sysone.fabricaAutomovil.model.Transacciones;

public interface TransaccionesRepository extends CrudRepository<Transacciones, Long>{

}
