package com.sysone.fabricaAutomovil.repository;

import org.springframework.data.repository.CrudRepository;

import com.sysone.fabricaAutomovil.model.Automovil;

public interface AutomovilRepositorio extends CrudRepository<Automovil, Long> {

}
