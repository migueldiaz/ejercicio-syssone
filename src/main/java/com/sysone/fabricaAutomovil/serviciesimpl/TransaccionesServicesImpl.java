package com.sysone.fabricaAutomovil.serviciesimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sysone.fabricaAutomovil.model.Transacciones;
import com.sysone.fabricaAutomovil.repository.TransaccionesRepository;
import com.sysone.fabricaAutomovil.services.TransaccionesServices;

@Service
@Transactional
public class TransaccionesServicesImpl implements TransaccionesServices {

	@Autowired
	TransaccionesRepository transaccionesRepository;

	@Override
	public List<Transacciones> getTransacciones() {
		return (List<Transacciones>) transaccionesRepository.findAll();
	}

	@Override
	public Transacciones getTransaccionesId(Long idtransaccion) {
		return transaccionesRepository.findById(idtransaccion).get();
	}

	@Override
	public void saveOrUpdateTransacciones(Transacciones transacciones) {
		transaccionesRepository.save(transacciones);

	}

	@Override
	public void deteleTransacciones(Long idtransaccion) {
		transaccionesRepository.deleteById(idtransaccion);

	}

}
