package com.sysone.fabricaAutomovil.serviciesimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sysone.fabricaAutomovil.model.Automovil;
import com.sysone.fabricaAutomovil.repository.AutomovilRepositorio;
import com.sysone.fabricaAutomovil.services.AutomovilServices;

@Service
@Transactional
public class AutomovilServicesImpl implements AutomovilServices {

	@Autowired
	AutomovilRepositorio automovilRepositorio;
	
	@Override
	public List<Automovil> getListaAutomovil() {
		return (List<Automovil>) automovilRepositorio.findAll();
	}

	@Override
	public Automovil getAutomovilId(Long idAutomovil) {
		return automovilRepositorio.findById(idAutomovil).get();
	}

	@Override
	public void saveOrUpdate(Automovil automovil) {
		automovilRepositorio.save(automovil);
		
	}

	@Override
	public void deleteAutomovilId(Long idAutomovil) {
		automovilRepositorio.deleteById(idAutomovil);
		
	}

}
