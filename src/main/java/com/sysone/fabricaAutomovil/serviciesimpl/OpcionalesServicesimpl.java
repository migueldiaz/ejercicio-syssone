package com.sysone.fabricaAutomovil.serviciesimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sysone.fabricaAutomovil.model.Opcionales;
import com.sysone.fabricaAutomovil.repository.OpcionalesRepository;
import com.sysone.fabricaAutomovil.services.OpcionalesServices;

@Service
@Transactional
public class OpcionalesServicesimpl implements OpcionalesServices{
	
	@Autowired
	OpcionalesRepository opcionalesRepository;

	@Override
	public List<Opcionales> getlistOpcionales() {
		return (List<Opcionales>) opcionalesRepository.findAll();
	}

	@Override
	public Opcionales getOpcionalesId(Long idOpcionales) {
		return opcionalesRepository.findById(idOpcionales).get();
	}

	@Override
	public void saveOrUpdateOpcionales(Opcionales opcionales) {
		opcionalesRepository.save(opcionales);
		
	}

	@Override
	public void deleteOpcionalesId(Long idOpcionales) {
		opcionalesRepository.deleteById(idOpcionales);
		
	}

}
