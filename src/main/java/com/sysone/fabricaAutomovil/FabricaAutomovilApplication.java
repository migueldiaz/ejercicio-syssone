package com.sysone.fabricaAutomovil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FabricaAutomovilApplication {

	public static void main(String[] args) {
		SpringApplication.run(FabricaAutomovilApplication.class, args);
	}

}
