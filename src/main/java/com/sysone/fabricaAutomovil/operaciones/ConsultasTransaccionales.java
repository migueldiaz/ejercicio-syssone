package com.sysone.fabricaAutomovil.operaciones;

import java.math.BigDecimal;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.sysone.fabricaAutomovil.model.Transacciones;

public class ConsultasTransaccionales {
	
	  private static Session sesion;

	     
	public BigDecimal totalCotizar(Transacciones transacciones){
		
		      			
		BigDecimal totalTransaccion= transacciones.getIdopcionales().getPreciovariante().add(transacciones.getIdautomovil().getPrecio());	
		
		return totalTransaccion ;
	}

}
