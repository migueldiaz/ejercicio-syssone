package com.sysone.fabricaAutomovil.services;

import java.util.List;

import com.sysone.fabricaAutomovil.model.Automovil;

public interface AutomovilServices {

	public List<Automovil> getListaAutomovil();
	
	public Automovil getAutomovilId (Long idAutomovil);
	
	public void saveOrUpdate (Automovil automovil);
	
	public void deleteAutomovilId (Long idAutomovil);
	
	
	
}
