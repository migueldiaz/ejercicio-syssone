package com.sysone.fabricaAutomovil.services;

import java.util.List;

import com.sysone.fabricaAutomovil.model.Opcionales;

public interface OpcionalesServices {

	public List<Opcionales> getlistOpcionales();
	
	public Opcionales getOpcionalesId (Long idopcionales);
	
	public void saveOrUpdateOpcionales (Opcionales opcionales);
	
	public void deleteOpcionalesId(Long idopcionales);
	
}
