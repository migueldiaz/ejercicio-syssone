package com.sysone.fabricaAutomovil.services;

import java.util.List;

import com.sysone.fabricaAutomovil.model.Transacciones;

public interface TransaccionesServices {
	
	public List<Transacciones> getTransacciones();
	
	public Transacciones getTransaccionesId(Long idtransaccion);
	
	public void saveOrUpdateTransacciones (Transacciones transacciones);
	
	public void deteleTransacciones (Long idtransaccion);

}
