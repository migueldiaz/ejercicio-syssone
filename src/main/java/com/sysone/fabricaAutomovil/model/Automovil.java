package com.sysone.fabricaAutomovil.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="automovil")
public class Automovil {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idautomovil")
	private Long idautomovil;
		

	@Column(name="modelo", unique=true)
	private String modelo;
	
	@Column(name="precio")
	private BigDecimal precio;

	public Automovil() {

	}

	public Automovil(Long idautomovil, String modelo, BigDecimal precio) {
		this.idautomovil = idautomovil;
		this.modelo = modelo;
		this.precio = precio;
	}

	public Long getIdautomovil() {
		return idautomovil;
	}

	public void setIdautomovil(Long idautomovil) {
		this.idautomovil = idautomovil;
	}


	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

}
