package com.sysone.fabricaAutomovil.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="transacciones")
public class Transacciones {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idtransaccion")
	private Long idtransaccion;
	
	@ManyToOne
	@JoinColumn(name="idautomovil")
	private Automovil idautomovil;
	
	@ManyToOne
	@JoinColumn(name="idopcionales")
	private Opcionales idopcionales;

	public Long getIdtransaccion() {
		return idtransaccion;
	}

	
	public void setIdtransaccion(Long idtransaccion) {
		this.idtransaccion = idtransaccion;
	}



	public Automovil getIdautomovil() {
		return idautomovil;
	}


	public void setIdautomovil(Automovil idautomovil) {
		this.idautomovil = idautomovil;
	}


	public Opcionales getIdopcionales() {
		return idopcionales;
	}


	public void setIdopcionales(Opcionales idopcionales) {
		this.idopcionales = idopcionales;
	}

	
	
	
	
}
