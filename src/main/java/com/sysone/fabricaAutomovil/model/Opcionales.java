package com.sysone.fabricaAutomovil.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "opcionales")
public class Opcionales {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idopcionales")
	private Long idopcionales;

	@Column(name = "tipovariante")
	private String tipovariante;
	
	@Column(name="abreviatura")
	private String abreviatura;

	@Column(name = "preciovariante")
	private BigDecimal preciovariante;

	public Opcionales() {

	}

	public Opcionales(Long idopcionales, String tipoVariante, BigDecimal precioVariante) {

		this.idopcionales = idopcionales;
		this.tipovariante = tipoVariante;
		this.preciovariante = precioVariante;
	}



	public Long getIdopcionales() {
		return idopcionales;
	}

	public void setIdopcionales(Long idopcionales) {
		this.idopcionales = idopcionales;
	}

	public String getTipovariante() {
		return tipovariante;
	}

	public void setTipovariante(String tipovariante) {
		this.tipovariante = tipovariante;
	}

	public BigDecimal getPreciovariante() {
		return preciovariante;
	}

	public void setPreciovariante(BigDecimal preciovariante) {
		this.preciovariante = preciovariante;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	
}
